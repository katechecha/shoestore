$(document).ready(function() {
    let cartItem = {};
    let cartItems = [];
    let arr = [{
            name: "Raf Simons x adidas",
            description: 'Mid - Platform Light-Up',
            size: [35, 36, 40],
            price: 760.00,
            colors: ["red", "pink", "blue"],
            img: "img/cross.png",
            id: 5
        },
        {
            name: "Raf Simons x adidas 2",
            description: 'Mid - Platform Light-Up 22',
            size: [44, 46, 45],
            price: 1200.00,
            colors: ["black", "white", "bege"],
            img: "img/cross2.png",
            id: 10
        }
    ];
    let countryDelivery = [{
            country: "Ukraine",
            delivery: ["PoshtaUkr $30", "PoshtaUkr $10", "PoshtaUkr $1", "PoshtaUkr $20"],
            deliveryPrice: [30, 10, 1, 20]
        },
        {
            country: "Russia",
            delivery: ["PoshtaRus $30", "PoshtaRus $10", "PoshtaRus $1", "PoshtaRus $20"]
        },
        {
            country: "USA",
            delivery: ["PostaUS $30", "PostaUS $10", "PoshtaUs $1", "PostaUS $20"]
        },
        {
            country: "Uzbekistan",
            delivery: ["PostaUzb $30", "PostaUzb $10", "PoshtaUzb $1", "PostaUzb $20"]
        },
        {
            country: "China",
            delivery: ["PostaChi $30", "PostaChi $10", "PoshtaChi $1", "PostaChi $20"]
        }
    ];
    let shoes = [{
            img: 'img/cross-product1.png',
            name: 'Supreme x Nike SB',
            description: 'Tennis Classic',
            price: '$100'
        },
        {
            img: 'img/cross-product1.png',
            name: 'Raf Simons x adidas',
            description: 'Hi top - Multi laces',
            price: '$200'
        },
        {
            img: 'img/cross-product1.png',
            name: 'AIR MAX TAVAS SD',
            description: 'Yeezy boost 350',
            price: '$300'
        },
        {
            img: 'img/cross-product1.png',
            name: 'Nike x Supreme',
            description: 'Air force one',
            price: '$400'
        }
    ]
    $(".information-inp").on("click", function() {
        $(".popup-menu-country").html("");
        countryDelivery.forEach((el, i) => {
            $(".popup-menu-country").append(`
                <li class="sub-menu country-sub-menu animate__fadeIn animate__animated animate__faster" data-id="${i}">${el.country}</li>
            `);
        });

        function funcCost(element) {
            let thisVal = element.text()
            let costShip = parseInt(thisVal.match(/\d+/), 10)
            $('.ship-cost').html(costShip)
        }

        $(".country-sub-menu").on("click", function() {
            let test = $(this).html();
            sizePicked = true;
            let dataIdd = $(this).attr("data-id");
            let dataDelivery = countryDelivery[dataIdd].delivery;
            $(".menu-align").html(test);
            $(".menu-delivery").html(dataDelivery[0]);
            $(".popup-menu-delivery").html("");
            dataDelivery.forEach((element, i) => {
                $(".popup-menu-delivery").append(`
                    <li class="sub-menu sub-menu-delivery animate__fadeIn animate__animated animate__faster" data-id="${i}">${element}</li>
                `);
            });
            funcCost($(".menu-delivery"))
            $(".sub-menu").on("click", function() {
                let test = $(this).html();
                $(this).parent().prev().prev().html(test);
                sizePicked = true;
                funcCost($(this))
            });
        });

    });
    $('.cross').on('click', function() {
        $('.popup').fadeOut()
    })
    countryDelivery[0].delivery.forEach((el, i) => {
        console.log(el + " " + countryDelivery[0].deliveryPrice[i])
    })
    arr.forEach((el, i) => {
        $(".footwear-model").append(`
            <img src="${el.img}" alt="" class="model-img" data-name="${el.name}" data-price="${el.price}" data-description="${el.description}" data-id="${i}" data-num="${el.id}">
        `);
        if (i === 0) {
            $(".model-img").addClass("active");
        }
    });

    $(".btn-buy").on("click", function() {
        let activeProduct = $(".model-img.active");
        let itemName = activeProduct.attr("data-name");
        let itemDescription = activeProduct.attr("data-description");
        let itemPrice = activeProduct.attr("data-price");
        let itemSrc = activeProduct.attr("src");
        let itemId = activeProduct.attr("data-num");
        let quantity = 1;
        let itemSize = $(".size-text").html();
        let isDuplicate = false;
        let duplicateNumber;
        if (itemSize === 'Size') {
            return false
        }
        $(".product-cart").html("")
        cartItems.forEach((e, i) => {
            if (e.src === itemSrc && e.size === itemSize) {
                isDuplicate = true
                duplicateNumber = i;
            }
        })
        if (isDuplicate) {
            cartItems[duplicateNumber].quantity++;
            cartItems[duplicateNumber].price = itemPrice * cartItems[duplicateNumber].quantity;
        } else {
            let product = {
                name: itemName,
                description: itemDescription,
                size: itemSize,
                price: itemPrice,
                singlePrice: itemPrice,
                src: itemSrc,
                quantity: quantity,
                id: itemId
            }
            cartItems.push(product);
        }

        let sumPrice = 0
        cartItems.forEach((element, i) => {
            $(".product-cart").append(`
            <div class="cart-items cart-items-${i}" data-number="${i}" data-num="${element.id}">
                    <div class="count-prod">
                        <p class="plus" data-price="${element.singlePrice}">+</p>
                        <p class="num">${element.quantity}</p>
                        <p class="minus" data-price="${element.singlePrice}">-</p>
                    </div>
                    <img src="${element.src}" alt="" style="width: 50px; height: 50px;">
                    <div class="cart-name">
                        <h2 class="product-name">${element.name}</h2>
                        <p class="product-text">${element.description}</p>
                    </div>
                    <p class="product-size">SIZE</p>
                    <p class="size-num">${element.size}</p>
                    <p class="product-price">$<span class="cost-price-${i}">${element.price}</span></p>
                    </div>
                `);

            sumPrice = sumPrice + Number(element.price)
        });
        $('.sub-cost').html(`${sumPrice}`)
        $('.plus').on('click', function() {
            let productNumber = $(this).parent().parent().attr("data-number")
            let singlePrice = $(this).attr('data-price');
            let costPrice = $('.cost-price-' + productNumber)
            costPrice.html(Number(singlePrice) + Number(costPrice.html()))
            let itemNum = $(this).next()
            itemNum.html(Number(itemNum.html()) + 1)
            let numPrice = $('.sub-cost')
            numPrice.html(Number(singlePrice) + Number(numPrice.html()))
            cartCounter++
            $(".cartCounter").html(`(${cartCounter})`).show()
            cartItems[productNumber].quantity++;
        })
        $('.minus').on('click', function() {
            let productNumber = $(this).parent().parent().attr("data-number")
            let itemCart = $('.cart-items-' + productNumber)
            let prodId = $(this).parent().parent().attr("data-num")

            let removalNumber = $(this).parent().parent().index()
            let singlePrice = $(this).attr('data-price');
            let costPrice = $('.cost-price-' + productNumber)
            costPrice.html(Number(costPrice.html()) - Number(singlePrice))
            let itemNum = $(this).prev()
            itemNum.html(Number(itemNum.html()) - 1)
            let numPrice = $('.sub-cost')
            numPrice.html(Number(numPrice.html()) - Number(singlePrice))
            cartCounter--
            $(".cartCounter").html(`(${cartCounter})`).show()
            cartItems[removalNumber].quantity--;
            if (cartItems[removalNumber].quantity < 1) {
                itemCart.remove();
                cartItems = cartItems.slice(0, removalNumber)
                if (cartCounter === 0) {
                    $('.product-cart, .ship-pay, .privacy, .confirm').css('display', 'none');
                    $('.text-none').css('display', 'block');
                }
            }

        })
    });

    let sizePicked = false;
    let cartCounter = 0;
    $(".model-img").on("click", function() {
        $(".model-img").removeClass("active");
        $(this).addClass("active");
        $(".size-text").html('Size')
        let dataId = $(this).attr("data-id");
        let dataSize = arr[dataId].size
        $(".menu-item").html("")
        dataSize.forEach(elem => {
            $(".menu-item").append(`
                <li class="sub-menu">${elem}</li>
            `)
        });
        $(".sub-menu").on("click", function() {
            $(".select-size").html($(this).find('.select-size').html());
            let test = $(this).html();
            // $(".size-text").html(test);
            $(this).parent().prev().prev().html(test);
            sizePicked = true;
        });
        let attrImg = $(this).attr("src").replace(".png", "");
        attrImg = attrImg + "-big.png";
        $(".bigfoot").attr("src", attrImg);
    });


    $(".menuJS").click(function() {
        $(this).find("ul").toggle();

    });
    $(".sub-menu").on("click", function() {
        $(".select-size").html($(this).find('.select-size').html());
        let test = $(this).html();
        // $(".size-text").html(test);
        $(this).parent().prev().prev().html(test);
        sizePicked = true;
    });
    $(".btn-buy").on("click", function() {
        let itemSize = $(".size-text").html();
        if (sizePicked === true && itemSize != 'Size') {
            cartCounter++;
            $(".cartCounter").html(`(${cartCounter})`).show();
        };
    });
    $(".footwear-like").on("click", function() {
        $(".like-white").toggleClass("fill-red", "fill-none");
    });
    $(".popup").on("click", function(e) {
        if (this == e.target) {
            $(this).fadeOut();
        };
    });
    $(".burger").on("click", function(e) {
        if (this == e.target) {
            $(this).fadeOut();
            $('.burger, .burger-points').fadeOut();
            $('.burger-container').removeClass('animate__animated animate__fadeInRight').addClass('animate__animated animate__fadeOutRight')
            $('.header, .footwear, .classic, .story, .social, .footer').css('filter', 'blur(0px)')
            $('.container').css('width', '90%')
            $('.header-container').css('margin-top', '50px')
            $('.menu-icon').removeClass('menu-icon-click clicked')
        };
    });
    // $('.menu-item').
    $(".header-cart").on("click", function() {
        $(".popup").fadeIn();
        if (cartCounter === 0) {
            $('.product-cart, .ship-pay, .privacy, .confirm').css('display', 'none');
            $('.text-none').css('display', 'block');
        } else {
            $('.product-cart, .ship-pay, .privacy, .confirm').css('display', 'flex');
            $('.text-none').css('display', 'none');
        }
    })

    $('#search-input').on('input', function(e) {
            let nameShoes = $(this).val();
            $('.product-container').html('')
            shoes.forEach(element => {
                if (element.name.toLowerCase().includes(nameShoes.toLowerCase())) {
                    $(".product-container").append(`
                    <a href="product-page.html">
                <div class="product-block-search">
                    <div class="product-block-container">
                        <div class="product-block_top">
                            <img src="${element.img}" alt="" class="cross-product">
                        </div>
                        <div class="product-block_bottom">
                            <h2 class="title-product">${element.name}</h2>
                            <p class="text-product">${element.description}</p>
                            <p class="price-product">${element.price}</p>
                        </div>
                    </div>
                </div>
                </a>
                `)
                }
            })
            $('.category-quantity').html(`(${$('.product-block-search').length})`)
        })
        // let blur = 

    $('.menu-icon').click(function() {
        $(this).toggleClass('clicked');
        if ($('.burger').css('display') == "none") {
            $('.burger, .burger-points').fadeIn();
            $('.burger-container').removeClass('animate__animated animate__fadeOutRight').addClass('animate__animated animate__fadeInRight')
            $('.header, .footwear, .classic, .story, .social, .footer, .subsection, .product, .section-search, .product, .section-category').css('filter', 'blur(5px)')
            $('.container').css('width', 'auto')
            $('.header-container').css('margin', '0')
            $('.menu-icon').addClass('menu-icon-click');
        } else {
            $('.burger, .burger-points').fadeOut();
            $('.burger-container').removeClass('animate__animated animate__fadeInRight').addClass('animate__animated animate__fadeOutRight')
            $('.header, .footwear, .classic, .story, .social, .footer, .subsection, .product, .section-search, .product, .section-category').css('filter', 'blur(0px)')
            $('.container').css('width', '90%')
            $('.header-container').css('margin-top', '50px')
            $('.menu-icon').removeClass('menu-icon-click')
        }
        // let mql = window.matchMedia('all and (max-width: 576px)');
        // if (mql.matches) {
        //     $('.header-container').css('margin-top', '30px')
        // } else {
        //     $('.header-container').css('margin-top', '50px')
        // }
    });
    // window.onscroll = function() { myFunction() };
    // var header = document.getElementsByClassName("menu-icon");
    // var sticky = header.offsetTop;

    // function myFunction() {
    //     if (window.pageYOffset > sticky) {
    //         header.classList.add("sticky");
    //     } else {
    //         header.classList.remove("sticky");
    //     }
    // }
    $(window).scroll(function() {
        let sticky = $('.menu-icon'),
            scroll = $(window).scrollTop();

        if (scroll >= 100) {
            sticky.addClass('fixed');
        } else {
            sticky.removeClass('fixed');
        }
    });

});